# Automatizando pruebas de infraestructura con Python y TestInfra

## Introducción

En estas instrucciones mostramos como usar la herramienta de test infra para automatizar la ejecución de pruebas de infraestructura de TI.
Esto permitirá integrar prácticas como CI/CD y mejorar la calidad de las liberaciones y despliegues.

### Objetivos

El objetivo de automatizar las prueba de infraestructura es la ejecución de tests descritos por QA y así probar de forma consitente elementos tales como:

 - Existencia y permisos sobre archivos y directorios.
 - Paquetes instalados.
 - Estado de servicios.
 - Puertos a la escucha.

### Prerequisitos

Se requieren los siguientes paquetes para la ejecución del proyecto sobre el host:

 - python3.5
 - python3-setuptools
 - pip3
 - Modulos python: [ansible, testinfra]

```
$ sudo apt-get install python3.5
$ sudo apt-get install python3-setuptools
$ sudo easy_install3 pip
$ sudo -H pip install -r requirements.txt
```

## Ejecución de los tests sobre un ambiente local

```
$ cd tester/testinfra
$ python3.5 runTests.py
```

Salida de la ejecución:

![Output](https://gitlab.com/kronops/images/raw/master/testinfra/img_out.png)

## Ejecución de tests sobre un host remoto con Ansible.

Se utiliza el backend de ansible y su inventario para la ejecución de pruebas sobre servidores remotos.
El script utiliza las siguientes variables basadas en una instalación de ansible y su inventario previamente instalada y configurada:

```
$ export PROJECT_ENV=example-dev
$ export PROJECT=example-local
$ export host=central-host
```

Existiendo por lo tanto el inventario: **/etc/ansible/inventory/example-dev/example-local**
Y la definición del host: **central-host**

Una vez realizada la configuración completa de ansible.

```
$ cd tester/testinfra
$ python3.5 runTests.py
```

## Ejecución de tests a través del deploy de K-maji enterprise

El orquestador se encarga de instalar el rol **tester-service**, el cual ya automatiza el despliegue de un servidor para automatización de pruebas de infraestructura y web.

A través de la interfaz de Jenkins que despliega el orquestador, se proceden a ejecutar los tests. El script genera un reporte XML para JUnit que es cargado y mostrado en Jenkins.

![Jenkins Pipeline](https://gitlab.com/kronops/images/raw/master/testinfra/img_ppl.png)

### Creación de nuevas pruebas de infraestructura

Para crear nuevas pruebas basta con definir un nuevo script dentro de la ruta:  **tester/testinfra/tests**

 * **Ver ejemplo: tests/example.py**

## Built With

* Python3[https://www.python.org/] - Programming language
* Testinfra[https://testinfra.readthedocs.io] - Testinfra Python unit test

## Authors

 * **Axel Herrera** - (axel.herrera@kronops.com.mx)
