#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class General(TestCase):
    def skip_test_pkgs(s):
        packages = ['cron']
        f.test_pkgs(s.host, packages, s.output)

    def test_services(s):
        services = ['crond']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)
