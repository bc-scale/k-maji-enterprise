#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Itop(TestCase):
    def test_services(s):
        services = ['httpd','mysqld']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)

    def test_pkgs(s):
        packages = [
         'httpd','MySQL-python','mysql-community-server','mysql-community-client',
         'php70','graphviz','php70-php-fpm','php70-php-json',
         'php70-php-mysqlnd','php70-php-pecl-zip']
        f.test_pkgs(s.host, packages, s.output)

    def test_ports(s):
        ports = ['127.0.0.1:3306/tcp',
                 ':::80/tcp']
        f.test_ports(s.host, ports, s.output)
