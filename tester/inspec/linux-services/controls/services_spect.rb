# encoding: utf-8

control 'Services' do
  title 'Check for services'
  describe service('crond') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe service('sshd') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  # describe service('acpid') do
  #   it { should be_installed }
  #   it { should be_enabled }
  #   it { should be_running }
  # end
end
