# encoding: utf-8

control 'Nginx package' do
  title 'Check nginx is installed'
  describe package('nginx') do
    it { should be_installed }
  end
end

control 'Check nginx config dir' do
  title 'Check nginx config dir'
  desc 'This directory should exists before running nginx.'
  describe file('/etc/nginx') do
    it { should be_directory }
    it { should be_owned_by 'root' }
    its(:group) { should match(/^root$/) }
  end
end

control 'Check nginx config file' do
  title 'Check nginx config file'
  desc 'This file should exists before running nginx.'
  describe file('/etc/nginx/nginx.conf') do
    it { should be_exist }
    it { should be_owned_by 'root' }
    its(:group) { should match(/^root$/) }
  end
end

control 'Nginx service' do
  title 'Check nginx is enabled and running'
  describe service('nginx') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

control 'Certbot package' do
  title 'Check certbot is installed'
  describe package('certbot') do
    it { should be_installed }
  end
end

control 'Check letsencrypt config dir' do
  title 'Check letsencrypt config dir'
  desc 'This directory should exists before running letsencrypt.'
  describe file('/etc/letsencrypt') do
    it { should be_directory }
    it { should be_owned_by 'root' }
    its(:group) { should match(/^root$/) }
  end
end

control 'Check letsencrypt accounts dir' do
  title 'Check letsencrypt accounts dir'
  desc 'This directory should exists before running letsencrypt.'
  describe file('/etc/letsencrypt/accounts') do
    it { should be_directory }
    it { should be_owned_by 'root' }
    its(:group) { should match(/^root$/) }
  end
end

control 'Check letsencrypt certs dir' do
  title 'Check letsencrypt certs dir'
  desc 'This directory should exists before running letsencrypt.'
  describe file('/etc/letsencrypt/certs') do
    it { should be_directory }
    it { should be_owned_by 'root' }
    its(:group) { should match(/^root$/) }
  end
end

control 'Check letsencrypt csr dir' do
  title 'Check letsencrypt csr dir'
  desc 'This directory should exists before running letsencrypt.'
  describe file('/etc/letsencrypt/csr') do
    it { should be_directory }
    it { should be_owned_by 'root' }
    its(:group) { should match(/^root$/) }
  end
end

control 'Check letsencrypt keys dir' do
  title 'Check letsencrypt keys dir'
  desc 'This directory should exists before running letsencrypt.'
  describe file('/etc/letsencrypt/keys') do
    it { should be_directory }
    it { should be_owned_by 'root' }
    its(:group) { should match(/^root$/) }
  end
end

