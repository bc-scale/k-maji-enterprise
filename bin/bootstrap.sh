#!/bin/bash

#
# script: bootstrap.sh
# author: jorge.medina@kronops.com.mx
# desc: Installs ansible on redhat from epel repo & prepares the environment for infrastructure, application and database deployments.

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/bootstrap.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# vars
source .env

# main
echo
echo "Installing basic requirements."
sudo yum -y install vim-enhanced curl git openssh openssh-clients redhat-lsb-core wget expect
echo

echo "Installing basic requirements."
sudo yum -y install epel-release
sudo yum -y check-update

echo
echo "Installing Ansible dependencies."
sudo yum -y install python python-pip PyYAML python-jinja2 python2-paramiko python2-crypto python-passlib sshpass ansible
sudo yum -y update ansible
echo

echo
echo "Installing python36"
sudo yum -y install python36 python36-devel python36-setuptools python36-pip
echo

echo
echo 'Installing pip modules'
sudo pip3.6 install requests==2.22.0 pytest-dotenv==0.4.0
echo

echo "Creating and changing ownership and permissions for /var/lib/ansible."
mkdir -p /var/lib/ansible/retries
chown -R root:root /var/lib/ansible
chmod 770 /var/lib/ansible/retries

echo "Changing ansible ownership and permissions for /var/log/ansible"
mkdir -p /var/log/ansible
chown root:root /var/log/ansible
chmod 770 /var/log/ansible
touch /var/log/ansible/ansible.log
chown root:root /var/log/ansible/ansible.log
chmod 660 /var/log/ansible/ansible.log

echo "Deploying Ansible infrastructure code."
rm -rf /etc/ansible
ln -s $PROJECT_ROOT/ansible /etc

echo "Cleaning previous Jenkins home installation."
[[ -d "/var/lib/jenkins"  ]] && cp -a /var/lib/jenkins /var/lib/jenkins-$(date +%Y_%m_%d_%S)
rm -rf /var/lib/jenkins

echo "Create docker auth folder"
mkdir $PROJECT_ROOT/.docker
echo "$DOCKER_PWD" > $PROJECT_ROOT/.docker/auth
