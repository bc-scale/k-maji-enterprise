#!/bin/bash

source /var/lib/jenkins/.env

HOST_GROUP="$1"
PROFILE="$2"
KEY=/etc/ansible/inventory/$PROJECT/.ssh/id-$PROJECT-ansible.rsa
user=root
INSPEC_OUT=$PROJECT_ROOT/tester/inspec/xmlresult
[[ -d "$WORKSPACE" ]] && OUT_PATH=$WORKSPACE || OUT_PATH=$INSPEC_OUT

if [[ ! -f "$KEY" ]]; then
    echo "No SSH KEY found"
    exit 1
fi
ansible all --limit $HOST_GROUP -m debug -a "var=ansible_ssh_host" > ips
cat ips | grep "ansible_ssh_host" | cut -f2 -d: > ips_tmp
sed 's/ //g' -i ips_tmp
sed 's/"//g' -i ips_tmp
sed 's/\x1b\[[0-9;]*m//g' -i ips_tmp
while read -r line || [[ -n "$line" ]]; do
  echo ''
  inspec exec $PROFILE -t ssh://$user@$line -i $KEY --reporter cli junit:$OUT_PATH/${line}-stdout.xml
done < ips_tmp
rm ips*
exit 0
