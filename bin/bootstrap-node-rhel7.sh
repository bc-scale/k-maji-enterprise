#!/bin/bash

#
# script: bootstrap-node.sh
# author: jorge.medina@kronops.com.mx
# desc: Installs basic requirements for ansible managed nodes.

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/bootstrap-node.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

# vars

# main

echo
echo "Updating yum lists."
sudo yum check-update
echo
echo "Installing basic requirements."
sudo yum -y install \
vim-enhanced curl git openssh openssh-clients redhat-lsb-core wget deltarpm
echo

echo "Installing basic requirements."
sudo yum -y install epel-release
sudo yum -y check-update

echo
echo "Installing Ansible dependencies."
sudo yum -y install PyYAML python-jinja2 python2-paramiko python2-crypto
echo
