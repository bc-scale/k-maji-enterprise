#!/bin/bash

set -e

echo -n "Host Password: "
read -s PWD
set -o allexport
source .env
set +o allexport
INVENTORY_PATH=/etc/ansible/inventory/$PROJECT/$PROJECT_ENV
PLAYBOOK=ansible/playbooks/deploy-newhost.yml

deployArtifacts() {
  printf "\n==> Deploy artifacts to TESTER\n"
  ansible-playbook $PLAYBOOK -e "TARGET=central_tester" --tags "copy_to_tester"
}

deployOneHost() {
  printf "\n==> Adding ansible HOST: %s IP: %s\n" "$HOST_NAME" "$HOST_IP"
  ansible-playbook $PLAYBOOK -e \
  "HOST_NAME=${HOST_NAME}
   HOST_IP=$HOST_IP
   INVENTORY_PATH=$INVENTORY_PATH
   TARGET=central_deployer" --skip-tags "copy_to_tester"
}

deployAllHosts() {
  while read line; do
    if [[ $line != '' ]]; then
      HOST_NAME=$(echo $line | cut -d ',' -f1)
      HOST_IP=$(echo $line | cut -d ',' -f2)
      ping -c 1 $HOST_IP
      if [[ "$?" == 0 ]]; then
        deployOneHost "$HOST_NAME" "$HOST_IP"
      else
        printf "\n==> [WARNING]: Unreachable Host $HOST_IP"
      fi
    fi
  done < $FILE
  printf "\n==> Deploying SSH Keys\n"
  bin/deploy-ansible-keys.sh
  deployArtifacts
}

# main
[[ ! -f "$INVENTORY_PATH" ]] && { echo "Not found $INVENTORY_PATH"; exit 1; }
if [[ "$#" -eq 1 ]]; then
  FILE=$1
  [[ -f "$FILE" ]] && deployAllHosts || { echo "Not found $FILE"; exit 1; }
elif [[ "$#" -eq 2 ]]; then
  HOST_NAME=$1
  HOST_IP=$2
  ping -c 1 $HOST_IP
  if [[ "$?" == 0 ]]; then
    bin/deploy-bootstrap-node.exp $HOST_IP $SSH_TMP_USER $PWD
    deployOneHost "$HOST_NAME" "$HOST_IP"
    bin/deploy-ansible-keys.sh
    deployArtifacts
  else
    printf "\n==> [WARNING]: Unreachable Host $HOST_IP"
    exit 1
  fi
else
  echo "[Error]: Bad argument!!"
  echo "Usage: bash bin/$0 <HOST_NAME> <IP>"
  echo "Usage: bash bin/$0 <HOSTSLIST.LST>"
  exit 1
fi

exit 0
