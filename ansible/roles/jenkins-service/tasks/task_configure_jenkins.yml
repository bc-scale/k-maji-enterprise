---

- name: Change shell for jenkins user
  user:
    name: jenkins
    shell: /bin/bash
    create_home: true
    home:  "{{ jenkins_home }}"
    state: present

- name: Ensure Jenkins is running and started at boot time
  service: name=jenkins state=started enabled=yes

- name: Configure Jenkins Port for EL
  lineinfile: dest={{ jenkins_init_file }} regexp=^JENKINS_PORT= line=JENKINS_PORT={{ jenkins_http_port }}
  register: config_changed

- name: Configure Jenkins default runtime settings
  template: src=default-jenkins.j2 dest={{ jenkins_init_file }} owner=root group=root mode=0644

- name: Restart jenkins now
  service: name=jenkins state=restarted
  when: config_changed.changed

- name: Create Jenkins init.groovy.d directory
  file:
    path: "{{ jenkins_home }}/init.groovy.d"
    owner: jenkins
    group: jenkins
    mode: 0750
    state: directory

- name: Setting groovy proxy settings
  template: src=proxy-settings.groovy.j2
            dest={{ jenkins_home }}/init.groovy.d/proxy-settings.groovy
            owner=jenkins group=jenkins mode=0640
  when: "lookup('env', 'http_proxy')"

- name: Setting groovy basic security for unattended installation
  template: src=basic-security.groovy.j2 dest={{ jenkins_home }}/init.groovy.d/basic-security.groovy owner=jenkins group=jenkins mode=0640

- name: Restart jenkins now
  service: name=jenkins state=restarted

- name: "{{ startup_delay_s | default(40) }}s delay while starting Jenkins"
  wait_for: host=localhost port={{ jenkins_http_port }} delay={{ startup_delay_s | default(40) }}

- name: "Create Jenkins CLI destination directory: {{ jenkins_dest }}"
  file: path={{ jenkins_dest }} state=directory

- name: Get Jenkins CLI
  get_url: url=http://localhost:{{ jenkins_http_port }}/jnlpJars/jenkins-cli.jar dest={{ jenkins.cli_dest }} mode=0440
  register: jenkins_local_cli
  until: "'OK' in jenkins_local_cli.msg or 'file already exists' in jenkins_local_cli.msg"
  retries: 5
  delay: 10

- name: Create Jenkins updates folder.
  file:
    path: "{{ jenkins_home }}/updates"
    owner: jenkins
    group: jenkins
    mode: 0755
    state: directory
  register: jenkins_plugins_folder_create

- name: Update Jenkins plugin data.
  shell: curl -L https://updates.jenkins-ci.org/update-center.json | sed '1d;$d' > "{{ jenkins_home }}/updates/default.json"
  args:
    creates: "{{ jenkins_home }}/updates/default.json"

- name: Permissions for default.json updates info.
  file:
    path: "{{ jenkins_home }}/updates/default.json"
    owner: jenkins
    group: jenkins
    mode: 0755
  when: jenkins_plugins_folder_create.changed

- name: Install Jenkins plugins
  jenkins_plugin:
    name: "{{ item }}"
    url: "http://localhost:{{ jenkins_http_port }}"
    url_username: "{{ jenkins_admin_username }}"
    url_password: "{{ jenkins_admin_password }}"
    timeout: 90
  with_items: "{{ jenkins_plugins }}"
  register: plugin_result
  until: plugin_result is success
  retries: 5
  delay: 4

- name: Stop Jenkins
  service: name=jenkins state=stopped
