itop-service
============

Tasks for setting up itop service.

Requirements
------------

EL based system.

Role Variables
--------------

apache_conf_file: /etc/httpd/conf/httpd.conf
apache_user: apache
apache_group: apache
apache_service_name: httpd
apache_sites_available_dir: /etc/httpd/sites-available
apache_sites_enabled_dir: /etc/httpd/sites-enabled
php_init_dir: /etc/php.d


Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
        - httpd-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
