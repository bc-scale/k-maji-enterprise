net-setup
=========

Network settings for ifcfg-eth0.

Requirements
------------

EL based system.

Role Variables
--------------

net_interfaces: [ 'eth0' ]

Dependencies
 -----------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - net-setup

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
