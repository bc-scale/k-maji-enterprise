kvm-service
===========

Deployment tasks for KVM Hypervisor on Ubuntu Server Host.

Requirements
------------

EL based system.

Role Variables
--------------

virt_backupdir: /var/lib/libvirt/vm-backups

Role Tags
--------------

 - kvm_service_initial
 - kvm_service

Dependencies
------------

kvm and qemu.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - kvm-service


You have to specify which tasks to run using the follow tags:

-

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: luis.carrillo@kronops.com.mx.
