openssh-service
=========

Tasks for setting up SSH Server.

Requirements
------------

EL based system.

Role Variables
--------------

None.

Dependencies
 -----------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - openssh-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
