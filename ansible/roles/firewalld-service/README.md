firewalld-service
=================

Tasks for setting up Firewalld service.

Requirements
------------

EL based system.

Role Variables
--------------

- firewalld_sources : For add new sources in firewalld configuration, please config follow vars list:

# Hosts Source
firewalld_sources:
  - '0.0.0.0/0'
  - '192.168.1.0/0'

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - firewalld-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jassiel.reyes@kronops.com.mx.
