nagios-service
==============

Tasks for setting up nagios server.

Requirements
------------

EL based system.

Role Variables
--------------

download_dir: "{{ ansible_env.HOME }}/nagios"
with_httpd_conf: /etc/httpd/conf.d
apache_user: apache
apache_service: httpd
nagios_version: 4.4.2
nagiosurl: "https://assets.nagios.com/downloads/nagioscore/releases/nagios-{{ nagios_version }}.tar.gz"
nagiossrc: "nagios-{{ nagios_version }}"
plugin_version: 2.2.1
pluginsurl: "https://www.nagios-plugins.org/download/nagios-plugins-{{ plugin_version }}.tar.gz"
pluginssrc: nagios-plugins-{{ plugin_version }}
nagios_path: /usr/local/nagios/
nagios_objects_path: /usr/local/nagios/etc/objects
nagios_servers_path: /usr/local/nagios/etc/servers
nagios_cfg_path: /usr/local/nagios/etc/nagios.cfg
monitoring_user: nagios
monitoring_command_group: nagcmd
nagios_users:
  - user: nagiosadmin
    pass: nagiosadmin
nrpeurl: "http://kent.dl.sourceforge.net/project/nagios/nrpe-2.x/nrpe-2.15/nrpe-{{ nrpe_version }}.tar.gz"
nrpesrc: nrpe-{{ nrpe_version }}
nrpe_version: 2.15

Dependencies
------------

This role depends on the follow roles:

 * httpd-service

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - nagios-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: axel.herrera@kronops.com.mx.
