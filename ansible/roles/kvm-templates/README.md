kvm-templates
=========

Tasks for provisioning kvm server templates.

Requirements
------------

EL based system.

Role Variables
--------------


# Only RHEL7.x
# #machine_type: pc-i440fx-rhel7.0.0
# # From RHEL6.x
machine_type: rhel6.0.0
emulator_path: /usr/libexec/qemu-kvm

vm_name: template-rhel7-x64-server
template_name: template-rhel7-x64-server
#template_name: template-oraclelinux7-x64-server
images_pool_name: /kvmImages
eth0_network_type: network
#eth0_network_type: bridge
eth0_network_name: default
#eth0_network_name: vnet0
# MACS Reservadas para template
# RH Template
eth0_mac: 52:54:00:f9:d4:9a
# OracleLinux Template
#eth0_mac: 52:54:00:f9:d4:9b

Dependencies
 -----------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - kvm-templates

License
-------

MIT

Author Information
------------------

Please any question, please contact the author Jorge Armando Medina.
